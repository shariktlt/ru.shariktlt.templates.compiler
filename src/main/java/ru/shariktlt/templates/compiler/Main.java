package ru.shariktlt.templates.compiler;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        CompilerCore compilerCore = new CompilerCore();
        compilerCore.compile();
    }
}
