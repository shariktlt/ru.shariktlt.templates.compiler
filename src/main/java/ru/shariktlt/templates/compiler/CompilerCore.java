package ru.shariktlt.templates.compiler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import freemarker.template.Configuration;
import ru.shariktlt.templates.compiler.dto.CompilerConfiguration;
import ru.shariktlt.templates.compiler.dto.ModelHandler;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CompilerCore {
    private final CompilerConfiguration compilerConfiguration;
    private final Configuration configuration;
    private Map<String, String> model;

    public CompilerCore() throws IOException {
        this(null);
    }

    public CompilerCore(String configFile) throws IOException {
        compilerConfiguration = (new ConfigurationReader().readConfiguration(configFile));
        configuration = new Configuration(Configuration.VERSION_2_3_27);
        configuration.setDefaultEncoding(compilerConfiguration.getEncoding());
        configuration.setDirectoryForTemplateLoading(new File(compilerConfiguration.getTemplateRoot()));
        configuration.setLogTemplateExceptions(compilerConfiguration.isLogTemplateExceptions());
        configuration.setWrapUncheckedExceptions(compilerConfiguration.isWrapUncheckedExceptions());
    }

    public boolean compile(){
        loadModel();
        processTemplates();
        return true;
    }

    /**
     * грузим файлы с переменными
     */
    private void loadModel() {
        model = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        for(String path : compilerConfiguration.getParameterSourceList()){
            File file = new File(path);
            if(file.exists()){
                try {
                    ModelHandler modelHandler = mapper.readValue(file, ModelHandler.class);
                    model.putAll(modelHandler.getMap());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void processTemplates() {
        System.out.println("process");
    }
}
