package ru.shariktlt.templates.compiler.dto;

import java.util.Collections;
import java.util.Map;

public class ModelHandler {
    Map<String,String> map = Collections.emptyMap();

    public ModelHandler() {
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
