package ru.shariktlt.templates.compiler.dto;

import java.util.Collections;
import java.util.List;

public class CompilerConfiguration {
    private String templateRoot;
    private String encoding;
    private boolean logTemplateExceptions = false;
    private boolean wrapUncheckedExceptions = true;
    private List<String> parameterSourceList = Collections.emptyList();

    public CompilerConfiguration() {
    }

    public String getTemplateRoot() {
        return templateRoot;
    }

    public void setTemplateRoot(String templateRoot) {
        this.templateRoot = templateRoot;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public boolean isLogTemplateExceptions() {
        return logTemplateExceptions;
    }

    public void setLogTemplateExceptions(boolean logTemplateExceptions) {
        this.logTemplateExceptions = logTemplateExceptions;
    }

    public boolean isWrapUncheckedExceptions() {
        return wrapUncheckedExceptions;
    }

    public void setWrapUncheckedExceptions(boolean wrapUncheckedExceptions) {
        this.wrapUncheckedExceptions = wrapUncheckedExceptions;
    }

    public List<String> getParameterSourceList() {
        return parameterSourceList;
    }

    public void setParameterSourceList(List<String> parameterSourceList) {
        this.parameterSourceList = parameterSourceList;
    }
}

